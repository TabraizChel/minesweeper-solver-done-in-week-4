
import itertools

def reConvert(sumCheck):
    sum1 = 0
    for i in sumCheck:
        if i == 0:
            continue
        else:
            sum1 += i - 1
            
            
def oneDArray(x):
    return list(itertools.chain(*x))

# reads in map as a matrix
def mapUpdateArray():
    currentMap = open('current_map.txt' , 'r')
    positions = []
    for line in currentMap:
        positionupdater = []
        positions.append(positionupdater)
        for i in line.split('|'):
           positionupdater.append(i)
    currentMap.close()       
         
        
    
    del positions[0] 
    for line in positions:
        line.pop(0)
        line.pop()
    return positions    
# reads in map as a lingle line using oneDArray(x)
def mapUpdateLine():
    currentMap = open('current_map.txt' , 'r')
    positions = []
    for line in currentMap:
        positionupdater = []
        positions.append(positionupdater)
        for i in line.split('|'):
           positionupdater.append(i)
    currentMap.close()       
         
        
    
    del positions[0] 
    for line in positions:
        line.pop(0)
        line.pop()
    return oneDArray(positions) 


def coordinate(d):
    cipher = { 'a1':1,  'b1:':2 ,'c1':3 , 'd1':4, 'e1':5, 'f1':6, 'g1':7, 'h1':8, 'i1':9,
               'a2':10 ,'b2':11,' c2':12 ,'d2':13 ,'e2':14 ,'f2':15 ,'g2':16 ,'h2':17 ,'i2':18,
               'a3':19 ,'b3':20 ,'c3':21 ,'d3':22 ,'e3':23 ,'f3':24 ,'g3':25, 'h3':26, 'i3':27,
               'a4':28 ,'b4':29 ,'c4':30 ,'d4':31 ,'e4':32 ,'f4':33 ,'g4':34 ,'h4':35 ,'i4':36,
               'a5':37, 'b5':38, 'c5':39, 'd5':40, 'e5':41, 'f5':42, 'g5':43, 'h5':44, 'i5':45,
               'a6':46, 'b6':47, 'c6':48, 'd6':49, 'e6':50, 'f6':51, 'g6':52, 'h6':53, 'i6':54,
               'a7':55, 'b7':56, 'c7':57, 'd7':58, 'e7':59, 'f7':60, 'g7':61, 'h7':62, 'i7':63,
               'a8':64, 'b8':65, 'c8':66, 'd8':67, 'e8':68, 'f8':69, 'g8':70, 'h8':71, 'i8':72,
               'a9':73, 'b9':74, 'c9':75, 'd9':76, 'e9':77, 'f9':78, 'g9':79, 'h9':80, 'i9':81}
    
    for i in cipher.keys():
        if cipher[i] == d:
            return i





positions = mapUpdateLine()
for i in range(len(positions)):
    if positions[i] == '   ':
        positions[i] = 0
    else:
        positions[i] = int(positions[i])+1    


holdIndex = []
holdSum = []
whereToPlay = {} 
j = 1
for i in range(len(positions)):
    if positions[i] == 0:
        if  j == 1:
            sumCheck = int(positions[i+1]) + int(positions[i+9]) + int(positions[i+10])
            holdSum.append(sumCheck)
            whereToPlay[sumCheck] = j
            
        elif j == 9:
            sumCheck = int(positions[i - 1]) + int(positions[i+8]) + int(positions[i+9])
            holdSum.append(sumCheck)
            whereToPlay[sumCheck] = j            
        elif j in (2,3,4,5,6,7,8):
            sumCheck = int(positions[i - 1]) + int(positions[i + 1]) + int(positions[i + 8])\
            + int(positions[i + 9]) + int(positions[i + 10])
            holdSum.append(sumCheck)
            whereToPlay[sumCheck] = j
            
        elif j in (74,75,76,77,78,79,80):
            sumCheck = int(positions[i - 1]) + int(positions[i + 1]) + int(positions[i - 8])\
            + int(positions[i - 9]) + int(positions[i - 10])
            holdSum.append(sumCheck)
            whereToPlay[sumCheck] = j
            
        elif j % 9 == 0 and j != 9 and j != 81:
            sumCheck = int(positions[i - 1]) + int(positions[i - 9]) + int(positions[i - 10])\
            + int(positions[i + 8]) + int(positions[i + 9])
            holdSum.append(sumCheck)
            whereToPlay[sumCheck] = j
            
        elif j % 9 == 1 and j != 73:
            sumCheck = int(positions[i - 9]) + int(positions[i - 8]) + int(positions[i + 1])\
            + int(positions[i + 9]) + int(positions[i + 10])
            holdSum.append(sumCheck)
            whereToPlay[sumCheck] = j
            
        elif j == 73:
             sumCheck = int(positions[i - 9]) + int(positions[i - 8]) + int(positions[i + 1])
             whereToPlay[sumCheck] = j
             
        elif j == 81:
             sumCheck = int(positions[i - 1]) + int(positions[i - 9]) + int(positions[i - 10])
             holdSum.append(sumCheck)
             whereToPlay[sumCheck] = j
        else:
             sumCheck = int(positions[i - 10]) + int(positions[i - 9]) + int(positions[i - 8]) \
             + int(positions[i - 1]) + int(positions[i + 1]) + int(positions[i+8])\
             + int(positions[i + 9]) + int(positions[i + 10])
             holdSum.append(sumCheck)
             whereToPlay[sumCheck] = j
    holdIndex.append(j)            
    j += 1  
    
    
print(whereToPlay)
print(coordinate(whereToPlay[sorted(whereToPlay)[0]]))

        
        
    
